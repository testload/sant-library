package cloud.testload
//DockerSwarm client libraries
import de.gesellix.docker.client.DockerClientImpl
import groovy.json.JsonSlurper
import groovy.xml.StreamingMarkupBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import static groovyx.net.http.ContentType.*

//for InfluxDB
import org.influxdb.InfluxDBFactory
import org.influxdb.InfluxDB
import org.influxdb.dto.Query
import org.influxdb.dto.QueryResult
import okhttp3.OkHttpClient

//for confluence
import groovyx.net.http.HTTPBuilder
import java.util.concurrent.TimeUnit
import static groovyx.net.http.ContentType.TEXT

//html parser
import org.jsoup.*
import org.jsoup.nodes.*
import groovy.json.JsonOutput

//markdown parser
import com.vladsch.flexmark.ast.Node
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension
import com.vladsch.flexmark.ext.tables.TablesExtension
import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.options.MutableDataSet

//k8s client libraries
import io.fabric8.kubernetes.api.model.EnvVar
import io.fabric8.kubernetes.api.model.Namespace
import io.fabric8.kubernetes.api.model.NamespaceBuilder
import io.fabric8.kubernetes.api.model.Quantity
import io.fabric8.kubernetes.api.model.ReplicationController
import io.fabric8.kubernetes.api.model.ReplicationControllerBuilder
import io.fabric8.kubernetes.api.model.ResourceQuota
import io.fabric8.kubernetes.api.model.ResourceQuotaBuilder
import io.fabric8.kubernetes.api.model.ResourceRequirements
import io.fabric8.kubernetes.api.model.ResourceRequirementsBuilder
import io.fabric8.kubernetes.api.model.Status
import io.fabric8.kubernetes.client.APIGroupNotAvailableException
import io.fabric8.kubernetes.client.Config
import io.fabric8.kubernetes.client.ConfigBuilder
import io.fabric8.kubernetes.client.DefaultKubernetesClient
import io.fabric8.kubernetes.client.KubernetesClient
import io.fabric8.kubernetes.client.KubernetesClientException
import io.fabric8.kubernetes.client.Watch
import io.fabric8.kubernetes.client.Watcher
import io.fabric8.kubernetes.client.internal.SerializationUtils

//clickhouse
import ru.yandex.clickhouse.ClickHouseDataSource
import ru.yandex.clickhouse.settings.ClickHouseProperties
import groovy.sql.Sql

class SANTLib {
    Script script
    //simple i18n :)
    def language = 'ru'
    def jksPassword = "111111"
    String dbName = "jmresults"
    def ruMap = ["num"                                : "№",
                 "name"                               : "Код процесса",
                 "intensityHour"                      : "Полученная интенсивность (операций/час)",
                 "intensitySec"                       : "Полученная интенсивность (операций/сек)",
                 "profileReached"                     : "Попадание в профиль (%)",
                 "avgRespTime"                        : "Среднее время отклика (секунд)",
                 "respTime97Perc"                     : "Время отклика 97% (не превышает секунд)",
                 "errorRate"                          : "Процент ошибок",
                 "wakingUp"                           : "Просыпаемся...",
                 "totalCountHTML"                     : "Общее количество<br />за время выполнения",
                 "intensityHourHTML"                  : "Полученная интенсивность<br />(операций/час)",
                 "intensitySecHTML"                   : "Полученная интенсивность<br />(операций/сек)",
                 "profileReachedHTML"                 : "Попадание в профиль<br />(%)",
                 "avgRespTimeHTML"                    : "Среднее время отклика<br />(секунд)",
                 "respTime97PercHTML"                 : "Время отклика 97%<br />(не превышает секунд)",
                 "scenarioName"                       : "Код сценария (ссылка на скрипт)",
                 "scenarioDescription"                : "Описание сценария",
                 "intensityPlannedPerHourPerContainer": "Интенсивность (операций в час на поток)",
                 "containers"                         : "Количество потоков",
                 "description"                        : "Описание профиля",
                 "starttime"                          : "Начало",
                 "stoptime"                           : "Завершение",
                 "holdload"                           : "Длительность(минут)",
                 "username"                           : "Пользователь",
                 "comment"                            : "Комментарий:",
                 "hlparse"                            : "Длительность(минут):",
                 "additionals"                        : "Дополнительно:"

    ]

    def enMap = ["num"                                : "#",
                 "name"                               : "Transaction",
                 "intensityHour"                      : "Intensity per hour",
                 "intensitySec"                       : "Intensity per second",
                 "profileReached"                     : "Percent profile reached",
                 "avgRespTime"                        : "Average response time",
                 "respTime97Perc"                     : "Response time 97 percentile",
                 "errorRate"                          : "Error rate",
                 "wakingUp"                           : "Waking up...",
                 "totalCountHTML"                     : "Total count",
                 "intensityHourHTML"                  : "Intensity<br />(ops/hour)",
                 "intensitySecHTML"                   : "Intensity<br />(ops/sec)",
                 "profileReachedHTML"                 : "Profile reached<br />(%)",
                 "avgRespTimeHTML"                    : "Average response time<br />(sec)",
                 "respTime97PercHTML"                 : "Response time 97 percentile<br />(sec)",
                 "scenarioName"                       : "Transaction (script URL)",
                 "scenarioDescription"                : "Transaction description",
                 "intensityPlannedPerHourPerContainer": "Planned intensity (per hour X per container)",
                 "containers"                         : "Containers",
                 "description"                        : "Profile description",
                 "starttime"                          : "Start time",
                 "stoptime"                           : "End time",
                 "holdload"                           : "Loading(minutes)",
                 "username"                           : "User name",
                 "comment"                            : "Commentary:",
                 "hlparse"                            : "Duration(minutes):",
                 "additionals"                        : "Additional:"
    ]

    def lazyListener=false
    def lazyListenerURL='https://gitlab.com/testload/jmeter-listener/raw/master/ListenerExample.jmx'

    def dateFormat = 'dd.MM.yyyy HH:mm'
    def outTimeZone = 'Europe/Moscow'
    def inTimeZone = 'UTC'
    def userName="Default User"

    def getTranslateFor(stringValue) {
        switch (this.language) {
            case "ru": return ruMap.get(stringValue)
            case "en": return enMap.get(stringValue)
        }
    }

    /**
     * --------------------------------------------------------------------------------------
     * Base part
     */

    /**
     * Waiting time (just wait based on JSON profile)
     * @param jsonText
     */
    def waitLoading(String jsonText) {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)

        int sleepTime = jsonProfile["Loading in minutes"].toInteger()
        int minutes = 0
        def awake = false
        while (minutes < sleepTime) {
            Thread.sleep(60000) { e ->
                assert e in InterruptedException
                System.out.println getTranslateFor("wakingUp")
                true // stop on sleeping
                awake = true
            }
            if (awake) break
            minutes++
            System.out.println ": " + minutes
        }
    }

    /**
     * One minute wait method
     * @return
     */
    def waitBuffer() {
        int sleepTime = 1
        int minutes = 0
        while (minutes < sleepTime) {
            Thread.sleep(60000)
            minutes++
        }
    }

    /**
     * --------------------------------------------------------------------------------------
     * Jenkins part
     */

    /**
     * Waiting time for Jenkins (just wait based on JSON profile) because mister J has own groovy parser
     * @param jsonText
     */
    def jenkinsWaitLoading(String jsonText) {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)

        int sleepTime = jsonProfile["Loading in minutes"].toInteger()
        script.sleep(60 * sleepTime)
    }

    /**
     *  One minute wait method for Jenkins
     * @return
     */
    def jenkinsWaitBuffer() {
        script.sleep(60)
    }

    /**
     * --------------------------------------------------------------------------------------
     * Confluence part
     */
    def confluenceCredentials
    def confluenceBasePageId
    def confluenceHost
    def confluenceSpace
    def confluenceListLimit = 25
    /**
     * Search page ID by page name in Confluence
     * @param profileName
     * @return ID
     */
    def confluenceGetProfileIdByName(profileName) {
        //получаем список дочерних страниц у базовой страницы (например "Профили нагрузочного тестирования")
        def client = new HTTPBuilder("${this.confluenceHost}${this.confluenceBasePageId}/child/page".toString())
        client.ignoreSSLIssues()
        client.setHeaders(Accept: 'application/json', Authorization: "Basic ${this.confluenceCredentials.bytes.encodeBase64().toString()}".toString())
        def theNode
        client.get(contentType: TEXT) {
            resp, reader ->
                theNode = new JsonSlurper().parseText(reader.text)['results'].find {
                    it['title'] == profileName
                }['id'].toString()
                URL aURL = new URL(this.confluenceHost);
                def profileURL = "${aURL.getProtocol()}://${aURL.getHost()}/pages/viewpage.action?pageId=${theNode}".toString()
                try {
                    script.echo profileURL
                }
                catch (Exception ex) {
                    println("Not script");
                }

        }
        return theNode
    }

    /**
     * Get page body by ID in Confluence form (HTML covered in JSON)
     * @param profileId
     * @return HTML Body
     */
    def confluenceGetProfileBodyById(profileId) {
        //получаем и обрабатываем JSON ответ
        def client = new HTTPBuilder("${this.confluenceHost}${profileId}?expand=body.storage".toString())
        client.ignoreSSLIssues()
        client.setHeaders(Accept: 'application/json', Authorization: "Basic ${this.confluenceCredentials.bytes.encodeBase64().toString()}".toString())
        def jsonRespBody
        client.get(contentType: TEXT) {
            resp, reader ->
                jsonRespBody = new JsonSlurper().parseText(reader.text)['body']['storage']['value'].toString()
        }
        return jsonRespBody
    }

    /**
     * Parse HTML body (with profile data) and convert to JSON
     * @param profileHTMLBody
     * @return out JSON profile body
     */
    def confluenceParseProfileBody(String profileHTMLBody) {
        Document doc = Jsoup.parse(profileHTMLBody, "", org.jsoup.parser.Parser.xmlParser())

        def hload = doc.select("*:contains(" + getTranslateFor("hlparse") + ")").last().text().split(":").last().trim()
        def description = doc.select("*[ac:name=excerpt]>ac|rich-text-body").html()
        def tableScripts = doc.select("table").get(0)

        def rowsScripts = tableScripts.select("tbody>tr")
        def listScripts = []
        rowsScripts.findAll { it.select("td") }.each { row ->
            def scriptLink = row.select("td:eq(0)").select("a[href]").attr("href").trim()
            def scriptName = row.select("td:eq(0)").text().trim()
            def scriptIntensity = row.select("td:eq(2)").text().trim()
            def scriptPacing = row.select("td:eq(3)").text().trim()
            def scriptThreads = row.select("td:eq(4)").text().trim()
            def scriptMemoryMb = row.select("td:eq(5)").text().trim()
            def jksName = row.select("td:eq(6)").text().trim()
            def restartPolicy = "any"
            if (scriptIntensity == 1) restartPolicy = "none"

            listScripts.add(["Script name"       : scriptName,
                             "Script link"       : scriptLink,
                             "Intensity per hour": scriptIntensity,
                             "Pacing in seconds" : scriptPacing,
                             "Containers"        : scriptThreads,
                             "Memory requirement": scriptMemoryMb,
                             "Restart policy"    : restartPolicy,
                             "JKS base filename" : jksName
            ])
        }

        if (doc.select("table").size() > 1) {
            def tableParams = doc.select("table").get(1)
            def rowsParams = tableParams.select("tbody>tr")
            def listParams = []
            rowsParams.findAll { it.select("td") }.each { row ->
                def paramName = row.select("td:eq(0)").text()
                def paramValue = row.select("td:eq(1)").text()

                listParams.add(["Parameter name" : paramName,
                                "Parameter value": paramValue
                ])
            }
            return JsonOutput.toJson("Loading in minutes": hload, "Description": description, "Scripts list": listScripts, "Parameters list": listParams)
        } else return JsonOutput.toJson("Loading in minutes": hload, "Description": description, "Scripts list": listScripts)
    }

    /**
     * Make HTML body with results for Confluence
     * @param jsonData
     * @param comments
     * @param username
     * @return
     */
    def confluenceBuildChildPageBody(String jsonData, comments, username = this.userName) {
        return confluenceBuildChildPageBodyWithAdditionalContent(jsonData,comments,username,"")
    }

    def confluenceBuildChildPageBodyWithAdditionalContent(String jsonData, comments, username = this.userName, String additionalContent) {
        def json = new JsonSlurper().parseText(jsonData)

        def mBuilder = new StreamingMarkupBuilder()
        mBuilder.encoding = 'UTF-8'

        def body = mBuilder.bind {
            /* description, if available */
            if (json["Description"]) {
                "ac:structured-macro"("ac:name": 'expand') {
                    parameter(name: 'title', getTranslateFor("description"))
                    "ac:rich-text-body" {
                        p {
                            mkp.yieldUnescaped(json["Description"])
                        }
                    }
                }
            }
            table /* briefly */ {
                thead {
                    tr {
                        th getTranslateFor("holdload")
                        th getTranslateFor("starttime")
                        th getTranslateFor("stoptime")
                        th getTranslateFor("username")
                    }
                }
                tbody {
                    tr {
                        td json["Duration"]
                        td json["Start"]
                        td json["Stop"]
                        td username
                    }
                }
            }
            table /*results*/ {
                thead {
                    tr {
                        th getTranslateFor("num")
                        th { mkp.yieldUnescaped(getTranslateFor("name")) }
                        th { mkp.yieldUnescaped(getTranslateFor("totalCountHTML")) }
                        th { mkp.yieldUnescaped(getTranslateFor("intensityHourHTML")) }
                        th { mkp.yieldUnescaped(getTranslateFor("intensitySec")) }
                        th { mkp.yieldUnescaped(getTranslateFor("profileReachedHTML")) }
                        th { mkp.yieldUnescaped(getTranslateFor("avgRespTimeHTML")) }
                        th { mkp.yieldUnescaped(getTranslateFor("respTime97PercHTML")) }
                        th { mkp.yieldUnescaped(getTranslateFor("errorRate")) }
                    }
                }
                tbody {
                    json["Results"].eachWithIndex { def row, int num ->
                        def profilePercent = row["Profile reached percent"]
                        def errRate = row["Errors rate"]
                        def name = row["Operation name"]
                        if (errRate.toInteger() > 0) {
                            errRate = "<span style='background-color: rgb(255,102,102);'>${row["Errors rate"]}</span>"
                        }
                        if (profilePercent.toFloat() < 98 && profilePercent.toInteger() != 0) {
                            profilePercent = "<ac:emoticon ac:name='cross'/>${row["Profile reached percent"]}"
                        } else if (profilePercent.toFloat() >= 98) {
                            profilePercent = "<ac:emoticon ac:name='tick' />${row["Profile reached percent"]}"
                        }
                        tr {
                            td num + 1
                            td name
                            td row["Total count"]
                            td row["Actual intensity"]
                            td row["Intensity per second"]
                            td { mkp.yieldUnescaped(profilePercent) }
                            td row["Average response time"]
                            td row["97percentile response time"]
                            td { mkp.yieldUnescaped(errRate) }
                        }
                    }
                }
            }
            /* comments, if available */
            if (comments) {
                p {
                    mkp.yieldUnescaped(getTranslateFor("comment"))
                }
                "ac:structured-macro"("ac:name": 'excerpt') {
                    parameter(name: 'hidden', false)
                    parameter(name: 'atlassian-macro-output-type', "BLOCK")
                    "ac:rich-text-body" {
                        p {
                            mkp.yieldUnescaped(comments)
                        }
                    }
                }
            }
            /* additionals, if available */
            if (additionalContent) {
                "ac:structured-macro"("ac:name": 'expand') {
                    parameter(name: 'title', getTranslateFor("additionals"))
                    "ac:rich-text-body" {
                        p {
                            mkp.yieldUnescaped(additionalContent)
                        }
                    }
                }

            }
        }
        return body.toString()
    }

    /**
     * Publish results page on Confluence
     * @param parentId
     * @param childName
     * @param childBody
     * @return
     */
    def confluencePublishChildPage(parentId, childName, childBody) {

        def packetJson = [
                ancestors: [[id: parentId]],
                body     : [
                        storage: [
                                value         : childBody,
                                representation: "storage"]
                ],
                space    : [key: this.confluenceSpace],
                type     : "page",
                title    : childName
        ]

        def client = new HTTPBuilder(this.confluenceHost)
        client.ignoreSSLIssues()
        //it necessary to set Content-Type because jenkins .....
        client.setHeaders(Authorization: "Basic ${this.confluenceCredentials.bytes.encodeBase64().toString()}".toString(),
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "*/*")

        def postRC = null
        client.request(Method.POST, ContentType.JSON) { req ->
            def jsonT = new groovy.json.JsonBuilder(packetJson)
            send URLENC, jsonT.toPrettyString()

            println "POST body: ${jsonT.toPrettyString()}"
            script.echo "POST body: ${jsonT.toPrettyString()}"

            response.success = { resp, json ->
                postRC = resp.statusLine.statusCode
                URL aURL = new URL(this.confluenceHost)

                def resultURL = "${aURL.getProtocol()}://${aURL.getHost()}/pages/viewpage.action?pageId=${json['id']}".toString()
                try {
                    script.echo resultURL
                }
                catch (Exception ex) {
                    println("Not script");
                }
                def jsonX = new groovy.json.JsonBuilder(json)
                script.echo "POST Pass: ${resp.statusLine.toString()}"
                script.echo "POST Pass: ${jsonX.toPrettyString()}"

                postRC = resp.statusLine.statusCode
                println "POST Pass: ${resp.statusLine.toString()}"
                println "POST Pass: ${jsonX.toPrettyString()}"
            }
            response.failure = { resp, json ->
                def jsonX = new groovy.json.JsonBuilder(json)
                script.echo "POST Fail: ${resp.statusLine.toString()}"
                script.echo "POST Fail: ${jsonX.toPrettyString()}"

                postRC = resp.statusLine.statusCode
                println "POST Fail: ${resp.statusLine.toString()}"
                println "POST Fail: ${jsonX.toPrettyString()}"
            }
        }

        return postRC
    }

    /**
     * List loading profiles from Confluence page
     * @return
     */
    def confluenceGetProfilesList() {
        def client = new HTTPBuilder("${this.confluenceHost}${this.confluenceBasePageId}/child/page?limit=${this.confluenceListLimit}".toString())
        client.ignoreSSLIssues()
        client.setHeaders(Accept: 'application/json', Authorization: "Basic ${this.confluenceCredentials.bytes.encodeBase64().toString()}".toString())
        def results
        client.get(contentType: TEXT) {
            resp, reader ->
                results = new JsonSlurper().parseText(reader.text)['results']['title']
        }
        return results
    }

    /**
     * Get page URL for ID
     * @return
     */

    def confluenceGetProfileWebURLById(profileId) {
        //получаем и обрабатываем JSON ответ
        def client = new HTTPBuilder("${this.confluenceHost}${profileId}?expand=body.view".toString())
        client.ignoreSSLIssues()
        client.setHeaders(Accept: 'application/json', Authorization: "Basic ${this.confluenceCredentials.bytes.encodeBase64().toString()}".toString())
        def jsonURL
        client.get(contentType: TEXT) {
            resp, reader ->
                URL aURL = new URL(this.confluenceHost);
                jsonURL = "${aURL.getProtocol()}://${aURL.getHost()}${new JsonSlurper().parseText(reader.text)['_links']['webui']}".toString()
                try {
                    script.echo jsonURL
                }
                catch (Exception ex) {
                    println("Not script");
                }
        }
        return jsonURL
    }

    /**
     * --------------------------------------------------------------------------------------
     * Docker part
     */
    def jmeterBaseImage
    def dockerAddress
    def loaderId
    def gitToken

    /**
     * Clean all services based on JSONProfile
     * @param jsonText
     * @return
     */
    def dockerCleanServices(String jsonText) {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)
        jsonProfile["Scripts list"].each
                {
                    if (!it["Script link"].toString().empty) {
                        System.out.println it["Script name"]
                        this.dockerRemoveService(cleanString(it["Script name"]))
                    }
                }
    }

    /**
     * Create services based on JSONProfile
     * @param profileName profile name
     * @param runId
     * @param jsonText
     * @return
     */
    def dockerCreateServices(profileName, runId, String jsonText) {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)

        jsonProfile["Scripts list"].eachWithIndex { def entry, int i ->
            System.out.println entry["Script name"]
            dockerCreateService(profileName, runId,
                    entry["Script name"],
                    jsonProfile["Loading in minutes"],
                    entry["Script link"],
                    entry["Pacing in seconds"],
                    entry["Containers"],
                    entry["Intensity per hour"],
                    entry["Memory requirement"].toInteger(),
                    entry["Restart policy"],
                    entry["JKS base filename"],
                    i.toString(),
                    jsonText)
        }
    }

    /**
     * delete Docker service based on name
     * @param serviceName
     * @return
     */
    def dockerRemoveService(serviceName) {
        if (this.dockerFindServiceByName(this.loaderId + cleanString(serviceName))) {
            def dockerClient = new DockerClientImpl(this.dockerAddress)
            dockerClient.rmService(this.loaderId + cleanString(serviceName))
        }
    }

    /**
     * Create one service
     * @param profileName
     * @param runId
     * @param serviceName
     * @param loadingTime
     * @param scriptAddress URL
     * @param pacing
     * @param copies containers scale
     * @param intensityPerHour
     * @param memoryLimitMegabytes
     * @param restartCondition
     * @param jksVar special parameters for JKS usage
     * @param jsonProfile json profile body for additional starting parameters
     * @return
     */
    def dockerCreateService(profileName, runId, serviceName, loadingTime, scriptAddress, pacing, copies, intensityPerHour, memoryLimitMegabytes = 270, restartCondition = "any", jksVar, scriptPosition, String jsonProfile) {
        //если URL скрипта пустой то не создаем ничего!
        if (!scriptAddress.isEmpty()) {
            if (!dockerFindServiceByName(serviceName)) {
                def slurper = new JsonSlurper()
                def profile = slurper.parseText(jsonProfile)
                def profileEnv = groovy.json.JsonOutput.toJson(profile) //format JSON profile for send to ENVs
                //системные переменные в контейнере
                def envs = [
                        "loadScriptURL=${scriptAddress}?private_token=${this.gitToken}".toString(),
                        "loadProfileName=-JProfileName=${profileName}".toString(),
                        "loadRunId=-JRunId=${runId}".toString(),
                        "loadIntencity=-JIntencity=${intensityPerHour}".toString(),
                        "loadHoldload=-JHoldload=${loadingTime}".toString(),
                        "loadPacing=-JPacing=${pacing}".toString(),
                        "loadHostName=-JHostName=DockerSwarm",
                        "loadInfluxHost=-JInfluxHostName=${this.influxHost}".toString(),
                        "loadInfluxPort=-JInfluxPort=${this.influxPort}".toString(),
                        "loadInfluxLogin=-JInfluxLogin=${this.influxLogin}".toString(),
                        "loadInfluxPassword=-JInfluxPassword=${this.influxPassword}".toString(),
                        "loadTaskSlot=-JTaskSlot={{.Task.Slot}}",
                        "containerNum={{.Task.Slot}}",
                        "nodeID={{.Node.ID}}",
                        "TZ=${this.outTimeZone}".toString(),
                        "UserName=${this.userName}".toString(),
                        "ProfileName=${profileName}".toString(),
                        "RunId=${runId}".toString(),
                        "ScriptName=${serviceName}".toString(),
                        "ScriptIntensity=${intensityPerHour}".toString(),
                        "ScriptURL=${scriptAddress}".toString(),
                        "RunType=DockerSwarm",
                        "ProfileJSON='$profileEnv'",
                        "Holdload=${loadingTime}".toString(),
                        "Pacing=${pacing}".toString(),
                        "ScriptPosition=${scriptPosition}".toString(),
                        "ClickHouseURL=${this.clickHouseUrl}".toString(),
                        "ClickHouseLogin=${this.clickHouseLogin}".toString(),
                        "ClickHousePassword=${this.clickHousePassword}".toString(),
                        "InfluxDBURL=${this.influxHost}:${this.influxPort}".toString(),
                        "InfluxDBLogin=${this.influxLogin}".toString(),
                        "InfluxDBPassword=${this.influxPassword}".toString(),
                        "LokiEndpoint=${this.lokiUrl}".toString(),
                        "LokiAuth=Basic ${this.lokiAuth.bytes.encodeBase64().toString()}",
                        "LazyListener=${this.lazyListener}".toString(),
                        "LazyListenerURL=${this.lazyListenerURL}".toString()
                ]
                if (!jksVar.isEmpty()) {
                    envs.add("jksFileBase=${jksVar}".toString())
                    envs.add("jksPassword=${this.jksPassword}".toString())
                }
                //обогащаем дополнительными, если есть чем
                profile["Parameters list"].eachWithIndex
                        { it, index ->
                            envs.add("param$index=-${it["Parameter name"]}=${it["Parameter value"]}".toString())
                        }

                def dockerClient = new DockerClientImpl(this.dockerAddress)
                def serviceConfig = [
                        "Name"        : this.loaderId + cleanString(serviceName),
                        "Labels"      : [
                                "servicetype"      : "testload",
                                "runid"            : runId,
                                "profilename"      : profileName,
                                "memoryrequirement": memoryLimitMegabytes.toString(),
                                "jobnum"           : this.loaderId
                        ],
                        "TaskTemplate": [
                                "ContainerSpec": [
                                        "Image"      : this.jmeterBaseImage,
                                        "Mounts": [
                                                ["ReadOnly": true,
                                                 "Source": "/etc/hostname",
                                                 "Target": "/tmp/hostname",
                                                 "Type": "bind"]
                                        ],
                                        "HealthCheck": ["Test": ["NONE"]],
                                        "Env"        : envs,
                                ],
                                "Resources"    : [
                                        "Limits"      : ["MemoryBytes": (memoryLimitMegabytes * 1000000).toInteger()],
                                        "Reservations": [:]
                                ],
                                "RestartPolicy": ["Condition": restartCondition],
                                "Placement"    : ["Constraints":
                                                          [
                                                                  "node.labels.testload ==true"
                                                          ]
                                ],
                                "Networks": [["Target":clickHouseNetwork]],
                        ],
                        "Mode"        : [
                                "Replicated": [
                                        "Replicas": copies.toInteger()
                                ]
                        ],
                        "UpdateConfig": [
                                "Parallelism": 1
                        ]]
                return dockerClient.createService(serviceConfig)
            }
            return null
        }
        return null
    }

    /**
     * Search service by name
     * @param serviceName
     * @return
     */

    def dockerFindServiceByName(serviceName) {
        def dockerClient = new DockerClientImpl(this.dockerAddress)
        def services = dockerClient.services().content
        return services.find { it.Spec.Name == cleanString(serviceName) }
    }

    /**
     * Search all services based on label
     * @param labelName
     * @param labelValue
     * @return
     */

    def dockerFindServiceByLabel(String labelName, String labelValue) {
        def dockerClient = new DockerClientImpl(this.dockerAddress)
        def services = dockerClient.services().content
        //конструкция несколько громоздкая потому что могут быть сервисы без Labels
        return services.findAll { it -> it.Spec.Labels }.findAll { it -> it.Spec.Labels[labelName] == labelValue }
    }

    /**
     * Search & clean all services based on label
     * @param labelName
     * @param labelValue
     * @return
     */
    def dockerFindServiceByLabelAndRemove(labelName, labelValue) {
        def list = dockerFindServiceByLabel(labelName, labelValue)
        def dockerClient = new DockerClientImpl(this.dockerAddress)
        list.each { it ->
            System.out.println it.Spec.Name
            dockerClient.rmService(it.Spec.Name)
        }
    }

    /**
     * --------------------------------------------------------------------------------------
     * InfluxDB part
     */
    def influxHost  = 'localhost'
    def influxPort  = '8086'
    def influxLogin ='jmeter'
    def influxPassword =''
    def influxTimeout = 600
    /**
     * Collect all results from InfluxDB
     * @param profileName
     * @param runId
     * @param jsonText planning profile
     * @return result json
     */
    def influxDBCollectResults(String profileName, String runId, String jsonText) {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)

        OkHttpClient.Builder builder = new OkHttpClient.Builder().readTimeout(this.influxTimeout, TimeUnit.SECONDS).connectTimeout(this.influxTimeout, TimeUnit.SECONDS)
        InfluxDB influxDB = InfluxDBFactory.connect("http://${this.influxHost}:${this.influxPort}".toString(), this.influxLogin, this.influxPassword, builder)
        influxDB.enableGzip()

        String dbName = "jmresults"
        Query query = new Query("SELECT sum(points) as intensity, mean(responseTime)/1000 as average, percentile(responseTime, 95)/1000 as resptime95, percentile(responseTime, 97)/1000 as resptime97, (sum(errorCount)/sum(points)) as errrate FROM requestsRaw  WHERE testName = '${profileName}' AND runId = '${runId}' GROUP BY requestName".toString(), dbName)

        QueryResult results = influxDB.query(query)
        influxDB.close()

        Query queryStop = new Query("SELECT last(points) FROM requestsRaw  WHERE testName = '${profileName}' AND runId = '${runId}' ORDER BY time".toString(), dbName)
        Query queryStart = new Query("SELECT first(points) FROM requestsRaw  WHERE testName = '${profileName}' AND runId = '${runId}' ORDER BY time".toString(), dbName)

        QueryResult resultsStart = influxDB.query(queryStart)
        QueryResult resultsStop = influxDB.query(queryStop)

        influxDB.close()

        def startDate = new Date()
        def stopDate = new Date()


        def parserDate = new java.text.SimpleDateFormat('yyyy-MM-dd\'T\'HH:mm:ss')
        parserDate.setTimeZone(TimeZone.getTimeZone(inTimeZone))

        def formatDate = new java.text.SimpleDateFormat(dateFormat)
        formatDate.setTimeZone(TimeZone.getTimeZone(outTimeZone))

        def startPoint = formatDate.format(startDate)
        def stopPoint = formatDate.format(stopDate)

        resultsStart.getResults().get(0).getSeries().each {
            startDate = parserDate.parse(it.getValues().get(0).get(0))
            startPoint = formatDate.format(startDate)
        }
        resultsStop.getResults().get(0).getSeries().each {
            stopDate = parserDate.parse(it.getValues().get(0).get(0))
            stopPoint = formatDate.format(stopDate)
        }

        def durationActual = (groovy.time.TimeCategory.minus(stopDate, startDate).toMilliseconds() / 1000 / 60).toLong()

        def jsonResult = []
        results.getResults().get(0).getSeries().each {
            def opName = it.tags.requestName
            def opCount = it.getValues().get(0).get(1)
            def opIntensity = opCount * (60 / jsonProfile["Loading in minutes"].toInteger())
            def opSecondIntensity = opIntensity / (3600) //intensity per second
            def opRespTimeAvg = it.getValues().get(0).get(2)
            def opRespTime95 = it.getValues().get(0).get(3)
            def opRespTime97 = it.getValues().get(0).get(4)
            def opErrorRate = it.getValues().get(0).get(5) * 100
            def opProfilePercent = 0.0
            def opPlannedIntensity = 0.0
            def plannedIntensityId = jsonProfile["Scripts list"].findIndexOf { it["Script name"] == opName }
            if (plannedIntensityId >= 0) {
                opPlannedIntensity = jsonProfile["Scripts list"].get(plannedIntensityId)["Intensity per hour"].toInteger() * jsonProfile["Scripts list"].get(plannedIntensityId)["Containers"].toInteger()
                if (opPlannedIntensity.toInteger() == 1) {
                    opIntensity = opCount * 1.0
                }
                opProfilePercent = (opIntensity / opPlannedIntensity * 100).round(1)
            }

            jsonResult.add(["Operation name": opName, "Total count": opCount.toString(), "Actual intensity": opIntensity.round(0), "Intensity per second": opSecondIntensity.round(0), "Average response time": opRespTimeAvg.round(2), "95percentile response time": opRespTime95.round(2), "97percentile response time": opRespTime97.round(2), "Errors rate": opErrorRate.round(1), "Profile reached percent": opProfilePercent, "Planned intensity": opPlannedIntensity])
        }
        return JsonOutput.toJson("Duration planned": jsonProfile["Loading in minutes"].toInteger(), "Duration": durationActual, "Start": startPoint, "Stop": stopPoint, "Description": jsonProfile["Description"], "Results": jsonResult)
    }

    /**
     * Return list of  results from InfluxDB based on profile name
     * @param profileName
     * @return
     */
    def influxDBGetRunsListByProfileName(profileName) {
        InfluxDB influxDB = InfluxDBFactory.connect("http://${this.influxHost}:${this.influxPort}".toString(), this.influxLogin, this.influxPassword)

        String dbName = "jmresults"
        Query query = new Query("SHOW TAG VALUES FROM requestsRaw WITH KEY = runId WHERE testName = '${profileName}'".toString(), dbName)

        QueryResult results = influxDB.query(query)
        influxDB.close()

        return results.getResults().get(0).getSeries().get(0).values.transpose()[1].reverse()
    }

    /**
     * --------------------------------------------------------------------------------------
     * ClickHouse part
     */
    def clickHouseUrl = 'localhost:8123'
    def clickHouseLogin = 'default'
    def clickHousePassword = ''
    def clickHouseTimeout = 600
    def clickHouseNetwork = 'clickhouse_ch_net'

    /**
     * Collect all results from clickHouse
     * @param profileName
     * @param runId
     * @param jsonText planning profile
     * @return result json
     */
    def clickHouseCollectResults(String profileName, String runId, String jsonText) {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)

        ClickHouseDataSource clickHouse
        ClickHouseProperties properties = new ClickHouseProperties()
        properties.setUser(this.clickHouseLogin)
        properties.setPassword(this.clickHousePassword)
        properties.setCompress(true)
        properties.setDecompress(true)
        properties.setMaxExecutionTime(this.clickHouseTimeout)
        clickHouse = new ClickHouseDataSource("jdbc:clickhouse://${this.clickHouseUrl}".toString(), properties)
        def sql = Sql.newInstance(clickHouse)
        def results = sql.rows("SELECT sample_label,\n" +
                "       sum(points_count) as \"Total\",\n" +
                "       count(points_count) as \"TotalRecs\",\n" +
                "       min(timestamp_sec) as start,\n" +
                "       max(timestamp_sec) as stop,\n" +
                "       toUInt64(stop - start) as period,\n" +
                "       TotalRecs/if(period>0, period, 1) as Rate,\n" +
                "       sum(points_count)/count(points_count) as \"Compress\",\n" +
                "       avg(average_time)/1000 as \"AvgResp\",\n" +
                "       min(average_time)/1000 as \"MinResp\",\n" +
                "       max(average_time)/1000 as \"MaxResp\",\n" +
                "       quantileTDigest(0.9)(average_time)/1000 as \"90perc\",\n" +
                "       quantileTDigest(0.95)(average_time)/1000 as \"95perc\",\n" +
                "       quantileTDigest(0.97)(average_time)/1000 as \"97perc\",\n" +
                "       quantileTDigest(0.99)(average_time)/1000 as \"99perc\",\n" +
                "       sum(errors_count) as \"Errors\",\n" +
                "       (sum(errors_count)/sum(points_count))*100 as \"ErrorRate\"\n" +
                "from ${this.dbName}\n" +
                "WHERE  profile_name ='${profileName}' and run_id='${runId}'\n" +
                "GROUP BY sample_label".toString())

        def resultsStartStop = sql.rows("SELECT min(timestamp_sec) start, max(timestamp_sec) stop\n" +
                "from ${this.dbName}\n" +
                "WHERE  profile_name ='${profileName}' and run_id='${runId}'".toString())

        def startDate = resultsStartStop.start.get(0)
        def stopDate = resultsStartStop.stop.get(0)

        def formatter = new java.text.SimpleDateFormat(dateFormat)
        formatter.setTimeZone(TimeZone.getTimeZone(outTimeZone))

        def startPoint = formatter.format(startDate)
        def stopPoint = formatter.format(stopDate)

        def durationActual = (groovy.time.TimeCategory.minus(stopDate, startDate).toMilliseconds() / 1000 / 60).toLong()

        def jsonResult = []
        results.each {
            def opName = it['sample_label']
            double opCount = it['Total'].toDouble()
            def opIntensity = opCount * (60 / jsonProfile["Loading in minutes"].toInteger())
            //def opSecondIntensity = opIntensity / (3600) //intensity per second
            def opRespTimeAvg = it['AvgResp']
            def opRespTime95 = it['95perc']
            def opRespTime97 = it['97perc']
            double opErrorRate = it['ErrorRate'].toDouble()
            def opProfilePercent = 0.0
            def opPlannedIntensity = 0.0
            def plannedIntensityId = jsonProfile["Scripts list"].findIndexOf { it["Script name"] == opName }
            if (plannedIntensityId >= 0) {
                opPlannedIntensity = jsonProfile["Scripts list"].get(plannedIntensityId)["Intensity per hour"].toInteger() * jsonProfile["Scripts list"].get(plannedIntensityId)["Containers"].toInteger()
                if (opPlannedIntensity.toInteger() == 1) {
                    opIntensity = opCount * 1
                }
                opProfilePercent = (opIntensity / opPlannedIntensity * 100).round(1)
            }

            jsonResult.add(["Operation name": opName, "Total count": opCount.toString(), "Actual intensity": opIntensity.round(0), "Intensity per second": it["Rate"].toDouble().round(2), "Average response time": opRespTimeAvg.round(2), "95percentile response time": opRespTime95.round(2), "97percentile response time": opRespTime97.round(2), "Errors rate": opErrorRate.round(1), "Profile reached percent": opProfilePercent, "Planned intensity": opPlannedIntensity])
        }
        return JsonOutput.toJson("Duration planned": jsonProfile["Loading in minutes"].toInteger(), "Duration": durationActual, "Start": startPoint, "Stop": stopPoint, "Description": jsonProfile["Description"], "Results": jsonResult)
    }

    /**
     * Collect all results from clickHouse
     * @param profileName
     * @param runId
     * @param jsonText planning profile
     * @return result json
     */
    def clickHouseCollectResultsForPeriodTimestamp(String profileName, String runId, String jsonText, String periodStartTimestamp, String periodEndTimestamp, String mask='') {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)

        ClickHouseDataSource clickHouse
        ClickHouseProperties properties = new ClickHouseProperties()
        properties.setUser(this.clickHouseLogin)
        properties.setPassword(this.clickHousePassword)
        properties.setCompress(true)
        properties.setDecompress(true)
        properties.setMaxExecutionTime(this.clickHouseTimeout)
        clickHouse = new ClickHouseDataSource("jdbc:clickhouse://${this.clickHouseUrl}".toString(), properties)
        def sql = Sql.newInstance(clickHouse)
        def results = sql.rows("SELECT sample_label,\n" +
                "       sum(points_count) as \"Total\",\n" +
                "       count(points_count) as \"TotalRecs\",\n" +
                "       min(timestamp_sec) as start,\n" +
                "       max(timestamp_sec) as stop," +
                "       toUInt64(stop - start) as period,\n" +
                "       TotalRecs/if(period>0, period, 1) as Rate,\n" +
                "       sum(points_count)/count(points_count) as \"Compress\",\n" +
                "       avg(average_time)/1000 as \"AvgResp\",\n" +
                "       min(average_time)/1000 as \"MinResp\",\n" +
                "       max(average_time)/1000 as \"MaxResp\",\n" +
                "       quantileTDigest(0.9)(average_time)/1000 as \"90perc\",\n" +
                "       quantileTDigest(0.95)(average_time)/1000 as \"95perc\",\n" +
                "       quantileTDigest(0.97)(average_time)/1000 as \"97perc\",\n" +
                "       quantileTDigest(0.99)(average_time)/1000 as \"99perc\",\n" +
                "       sum(errors_count) as \"Errors\",\n" +
                "       (sum(errors_count)/sum(points_count))*100 as \"ErrorRate\"\n" +
                "from ${this.dbName}\n" +
                "WHERE timestamp_millis BETWEEN ${periodStartTimestamp} AND ${periodEndTimestamp} AND profile_name ='${profileName}' and run_id='${runId}' and match(sample_label, '${mask}')\n" +
                "GROUP BY sample_label\n" +
                "ORDER BY sample_label".toString())

        def resultsStartStop = sql.rows("SELECT min(timestamp_sec) start, max(timestamp_sec) stop\n" +
                "from ${this.dbName}\n" +
                "WHERE  timestamp_millis BETWEEN ${periodStartTimestamp} AND ${periodEndTimestamp} AND profile_name ='${profileName}' and run_id='${runId}'".toString())

        def startDate = resultsStartStop.start.get(0)
        def stopDate = resultsStartStop.stop.get(0)

        def formatter = new java.text.SimpleDateFormat(dateFormat)
        formatter.setTimeZone(TimeZone.getTimeZone(outTimeZone))

        def startPoint = formatter.format(startDate)
        def stopPoint = formatter.format(stopDate)

        def durationActual = (groovy.time.TimeCategory.minus(stopDate, startDate).toMilliseconds() / 1000 / 60).toLong()

        def jsonResult = []
        results.each {
            def opName = it['sample_label']
            double opCount = it['Total'].toDouble()
            def opIntensity = opCount * (60 / durationActual)
            //def opSecondIntensity = opIntensity / (3600) //intensity per second
            def opRespTimeAvg = it['AvgResp']
            def opRespTime95 = it['95perc']
            def opRespTime97 = it['97perc']
            double opErrorRate = it['ErrorRate'].toDouble()
            def opProfilePercent = 0.0
            def opPlannedIntensity = 0.0
            def plannedIntensityId = jsonProfile["Scripts list"].findIndexOf { it["Script name"] == opName }
            if (plannedIntensityId >= 0) {
                opPlannedIntensity = jsonProfile["Scripts list"].get(plannedIntensityId)["Intensity per hour"].toInteger() * jsonProfile["Scripts list"].get(plannedIntensityId)["Containers"].toInteger()
                if (opPlannedIntensity.toInteger() == 1) {
                    opIntensity = opCount * 1
                }
                opProfilePercent = (opIntensity / opPlannedIntensity * 100).round(1)
            }

            jsonResult.add(["Operation name": opName, "Total count": opCount.toString(), "Actual intensity": opIntensity.round(0), "Intensity per second": it["Rate"].toDouble().round(2), "Average response time": opRespTimeAvg.round(2), "95percentile response time": opRespTime95.round(2), "97percentile response time": opRespTime97.round(2), "Errors rate": opErrorRate.round(1), "Profile reached percent": opProfilePercent, "Planned intensity": opPlannedIntensity])
        }
        return JsonOutput.toJson("Duration planned": jsonProfile["Loading in minutes"].toInteger(), "Duration": durationActual, "Start": startPoint, "Stop": stopPoint, "Description": jsonProfile["Description"], "Results": jsonResult)
    }

    /**
     * Return list of  results from ClickHouse based on profile name
     * @param profileName
     * @return
     */
    def clickHouseGetRunsListByProfileName(profileName) {
        ClickHouseDataSource clickHouse
        ClickHouseProperties properties = new ClickHouseProperties()
        properties.setUser(this.clickHouseLogin)
        properties.setPassword(this.clickHousePassword)
        properties.setCompress(true)
        properties.setDecompress(true)
        clickHouse = new ClickHouseDataSource("jdbc:clickhouse://${this.clickHouseUrl}".toString(), properties)
        def sql = Sql.newInstance(clickHouse)
        return sql.rows("SELECT DISTINCT run_id FROM ${this.dbName} where profile_name='${profileName}'".toString())['run_id']
    }

    /**
     * --------------------------------------------------------------------------------------
     * GitlabWiki part
     */

    def gitlabWikiHost = "https://gitlab.com/api/v4/projects/"
    def gitlabWikiProjectId //"1111111" ProjectID
    def gitlabWikiBasePageTitle = "profiles"

    /**
     * Get profile body in MarkDown form based on profile name
     * @param profileId
     * @return HTML body
     */
    def gitlabWikiGetProfileBodyByName(profileName) {
        def client = new HTTPBuilder("${this.gitlabWikiHost}${this.gitlabWikiProjectId}/wikis/${this.gitlabWikiBasePageTitle}%2F${profileName}".toString())
        client.setHeaders(Accept: 'application/json', 'PRIVATE-TOKEN': this.gitToken)
        def jsonContent
        client.get(contentType: TEXT) {
            resp, reader ->
                jsonContent = new JsonSlurper().parseText(reader.text)['content'].toString()
        }
        return jsonContent
    }

    /**
     * Parse MD profile table and make JSONProfileBody
     * @param profileMDBody
     * @return output JSON
     */
    def gitlabWikiParseProfileBody(String profileMDBody) {
        MutableDataSet options = new MutableDataSet()
        options.set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create(), StrikethroughExtension.create()))
        options.set(HtmlRenderer.SOFT_BREAK, "<br />\n")
        Parser parser = Parser.builder(options).build()
        HtmlRenderer renderer = HtmlRenderer.builder(options).build()
        Node document = parser.parse(profileMDBody)
        String htmlBody = renderer.render(document)

        Document doc = Jsoup.parse(htmlBody)

        //get first table on "page" - profile
        def tableScripts = doc.select("tbody").get(0)
        def PP = doc.select("p")

        def hlNum = PP.findIndexOf {
            it.text().contains(getTranslateFor("hlparse"))
        }
        def hload = 0
        def description = ""
        if (hlNum >= 0) {
            hload = PP.get(hlNum).text().split(':')[1]
            if (hlNum > 0) {
                (0..hlNum - 1).each {
                    description = description + PP.get(it).text() + "\n"
                }
            }
        }

        def rowsScripts = tableScripts.select("tr")
        def listScripts = []
        rowsScripts.each { row ->
            def scriptLink = row.select("td:eq(0)").select("a[href]").attr("href").trim()
            def scriptName = row.select("td:eq(0)").text().trim()
            def scriptIntensity = row.select("td:eq(2)").text().trim()
            def scriptPacing = row.select("td:eq(3)").text().trim()
            def scriptThreads = row.select("td:eq(4)").text().trim()
            def scriptMemoryMb = row.select("td:eq(5)").text().trim()
            def jksName = row.select("td:eq(6)").text().trim()
            def restartPolicy = "any"
            if (scriptIntensity == 1) restartPolicy = "none"

            listScripts.add(["Script name"       : scriptName,
                             "Script link"       : scriptLink,
                             "Intensity per hour": scriptIntensity,
                             "Pacing in seconds" : scriptPacing,
                             "Containers"        : scriptThreads,
                             "Memory requirement": scriptMemoryMb,
                             "Restart policy"    : restartPolicy,
                             "JKS base filename" : jksName
            ])
        }

        if (doc.select("tbody").size() > 1) {
            def tableParams = doc.select("tbody").get(1)
            def rowsParams = tableParams.select("tr")
            def listParams = []
            rowsParams.each { row ->
                def paramName = row.select("td:eq(0)").text()
                def paramValue = row.select("td:eq(1)").text()

                listParams.add(["Parameter name" : paramName,
                                "Parameter value": paramValue
                ])
            }
            return JsonOutput.toJson("Loading in minutes": hload, "Description": description, "Scripts list": listScripts, "Parameters list": listParams)
        } else return JsonOutput.toJson("Loading in minutes": hload, "Description": description, "Scripts list": listScripts)
    }

    /**
     * List testload profiles from GitlabWiki page
     * @return
     */
    def gitlabWikiGetProfilesList(profilesDirectoryName = "profiles") {
        def client = new HTTPBuilder("${this.gitlabWikiHost}${this.gitlabWikiProjectId}/wikis/".toString())
        client.ignoreSSLIssues()
        client.setHeaders(Accept: 'application/json', 'PRIVATE-TOKEN': this.gitToken)
        def listTitles
        client.get(contentType: TEXT) {
            resp, reader ->
                listTitles = new JsonSlurper().parseText(reader.text).findAll {
                    it.slug.contains(profilesDirectoryName)
                }['title']
        }
        return listTitles
    }


    /**
     * Make GitlabWiki body page with results just from Confluence HTML
     * @param jsonData
     * @param comments
     * @return
     */
    def gitlabWikiBuildChildPageBody(String jsonData, comments, username = this.userName) {
        return gitlabWikiBuildChildPageBodyWithAdditionalContent(jsonData,comments,username,"")
    }

    def gitlabWikiBuildChildPageBodyWithAdditionalContent(String jsonData, comments, username = this.userName, String additionalContent) {
        def htmlBlob = confluenceBuildChildPageBodyWithAdditionalContent(jsonData, comments, username,additionalContent)
        //few cleans for HTML
        def returnedHtml = htmlBlob
                .replaceAll("</ac:rich-text-body></ac:structured-macro>", "")
                .replaceAll("<ac:structured-macro ac:name='expand'><parameter name='title'>${getTranslateFor("description")}</parameter><ac:rich-text-body>", " **${getTranslateFor("description")}** ")
                .replaceAll("<ac:structured-macro ac:name='excerpt'><parameter name='hidden'>false</parameter><parameter name='atlassian-macro-output-type'>BLOCK</parameter><ac:rich-text-body>", "")
                .replaceAll("<ac:emoticon ac:name='tick' />", ":white\\_check\\_mark: ")
                .replaceAll("<td><span style='background-color:\\ rgb\\(255,102,102\\);'>", "<td>:warning: ")
                .replaceAll("</span></td>", "</td>")
        return returnedHtml
    }

    /**
     * Make Markdown body with results!
     * @param jsonData
     * @param comments
     * @return
     */
    def gitlabWikiBuildChildPageBodyDirectly(String jsonData, comments, username = "Default User") {
        return gitlabWikiBuildChildPageBodyDirectlyWithAdditionalContent(jsonData,comments,username,"")
    }

    def gitlabWikiBuildChildPageBodyDirectlyWithAdditionalContent(String jsonData, comments, username = "Default User",String additionalContent) {
        def json = new JsonSlurper().parseText(jsonData)
        def description = json["Description"]
        def holdload = json["Duration"]
        def start_time = json["Start"]
        def end_time = json["Stop"]

        def header = ""
        if (description) header = " ${getTranslateFor("description")} ${description}\n\n"

        def briefInfo = "| **${getTranslateFor("holdload")}** " +
                "| **${getTranslateFor("starttime")}** " +
                "| **${getTranslateFor("stoptime")}** " +
                "| **${getTranslateFor("username")}** |\n" +
                "| ------ | ------ | ------ | ------ |\n" +
                "|${holdload}|${start_time}|${end_time}|${username}|\n\n"

        def tableResult = "| **${getTranslateFor("num")}** " +
                "| **${getTranslateFor("name")}** " +
                "| **${getTranslateFor("intensityHour")}** " +
                "| **${getTranslateFor("intensitySec")}** " +
                "| **${getTranslateFor("profileReached")}** " +
                "| **${getTranslateFor("avgRespTime")}** " +
                "| **${getTranslateFor("respTime97Perc")}** " +
                "| **${getTranslateFor("errorRate")}** |\n" +
                "| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |\n"

        json["Results"].eachWithIndex { def it, int num ->
            def profilePercent = it["Profile reached percent"]
            def errRate = it["Errors rate"]
            def name = it["Operation name"]
            if (errRate.toInteger() > 0) {
                errRate = ":warning: ${it["Errors rate"]}"
            }
            if (profilePercent.toDouble() < 98 && profilePercent.toInteger() != 0) {
                profilePercent = ":exclamation: ${it["Profile reached percent"]}"
            } else if (profilePercent.toDouble() >= 98) {
                profilePercent = ":white\\_check\\_mark: ${it["Profile reached percent"]}"
            }
            tableResult += "|${num + 1}|${name}|${it["Actual intensity"]}|${it["Intensity per second"]}|${profilePercent}|${it["Average response time"]}|${it["97percentile response time"]}|${errRate}|\n"
        }
        def footer = ""
        if (comments) footer = "\n${getTranslateFor("comment")} ${comments}\n\n"
        if (additionalContent) footer = "\n${getTranslateFor("additionals")} ${additionalContent}\n\n"
        return "${header}${briefInfo}${tableResult}${footer}"
    }

    /**
     * Publish results page on GitlabWiki
     * @param parentId
     * @param childName
     * @param childBody
     * @return
     */
    def gitlabWikiPublishChildPage(childName, childBody, resultsDirectoryName = "results") {
        def postBody = [format : "markdown",
                        title  : resultsDirectoryName + "/" + childName,
                        content: childBody
        ]
        def client = new HTTPBuilder("${this.gitlabWikiHost}${this.gitlabWikiProjectId}/wikis/".toString(), ContentType.URLENC)
        client.ignoreSSLIssues()
        def postRC = null
        client.request(Method.POST) { req ->
            headers['PRIVATE-TOKEN'] = this.gitToken
            body = postBody
            response.success = { resp, json ->
                postRC = resp.statusLine.statusCode
                println "POST Pass: ${resp.statusLine}"
            }
            response.failure = { resp, json ->
                postRC = resp.statusLine.statusCode
                println "POST Fail: ${resp.statusLine}"
            }
        }
        return postRC
    }

    /**
     * --------------------------------------------------------------------------------------
     * k8s part
     */
    def k8sAPIURL
    def k8sToken
    def k8sCAcert
    def k8snamespace = "testload"

    /**
     * Clean all services in Namespace
     * @param jsonText
     * @return
     */
    def k8sCleanAllServices() {
        Config config = new ConfigBuilder().withMasterUrl(this.k8sAPIURL).withCaCertData(this.k8sCAcert).withOauthToken(this.k8sToken).build()
        KubernetesClient client = new DefaultKubernetesClient(config)
        client.replicationControllers().inNamespace(this.k8snamespace).delete()
    }

    /**
     * Clean all services based on JSONProfile
     * @param jsonText
     * @return
     */
    def k8sCleanServices(jsonText) {
        Config config = new ConfigBuilder().withMasterUrl(this.k8sAPIURL).withTrustCerts(true).withCaCertData(this.k8sCAcert).withOauthToken(this.k8sToken).build()
        KubernetesClient client = new DefaultKubernetesClient(config)
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)
        jsonProfile["Scripts list"].each
                {
                    if (!it["Script link"].toString().empty) {
                        System.out.println it["Script name"]
                        client.replicationControllers().inNamespace(this.k8snamespace).withName(this.loaderId + cleanString(it["Script name"])).delete()
                    }
                }
    }

    /**
     * Clean all services based on JSONProfile
     * @param jsonText
     * @return
     */
    def k8sCleanServicesByLoaderId() {
        Config config = new ConfigBuilder().withMasterUrl(this.k8sAPIURL).withTrustCerts(true).withCaCertData(this.k8sCAcert).withOauthToken(this.k8sToken).build()
        KubernetesClient client = new DefaultKubernetesClient(config)
        client.replicationControllers().inNamespace(this.k8snamespace).withLabelIn('jobnum', this.loaderId).delete()
    }

    /**
     * Create pods based on JSONProfile
     * @param profileName profile name
     * @param runId
     * @param jsonText
     * @return
     */
    def k8sCreateServices(profileName, runId, String jsonText) {
        def slurper = new JsonSlurper()
        def jsonProfile = slurper.parseText(jsonText)

        jsonProfile["Scripts list"].eachWithIndex { def entry, int i ->
            System.out.println entry["Script name"]
            k8sCreateService(profileName, runId,
                    entry["Script name"],
                    jsonProfile["Loading in minutes"],
                    entry["Script link"],
                    entry["Pacing in seconds"],
                    entry["Containers"],
                    entry["Intensity per hour"],
                    entry["Memory requirement"].toInteger(),
                    entry["Restart policy"],
                    entry["JKS base filename"],
                    i.toString(),
                    jsonText)
        }
    }

    /**
     * delete k8s pods based on name
     * @param serviceName
     * @return
     */
    def k8sRemoveService(serviceName) {
        Config config = new ConfigBuilder().withMasterUrl(this.k8sAPIURL).withTrustCerts(true).withCaCertData(this.k8sCAcert).withOauthToken(this.k8sToken).build()
        KubernetesClient client = new DefaultKubernetesClient(config)
        client.replicationControllers().inNamespace(this.k8snamespace).withName(serviceName).delete()
    }

    /**
     * Create one service on k8s
     * @param profileName
     * @param runId
     * @param serviceName
     * @param loadingTime
     * @param scriptAddress URL
     * @param pacing
     * @param copies containers scale
     * @param intensityPerHour
     * @param memoryLimitMegabytes
     * @param restartCondition
     * @param jksVar special parameters for JKS usage
     * @param jsonProfile json profile body for additional starting parameters
     * @return
     */
    def k8sCreateService(profileName, runId, serviceName, loadingTime, scriptAddress, pacing, copies, intensityPerHour, memoryLimitMegabytes = 270, restartCondition = "any", jksVar, scriptPosition, jsonProfile) {
        //if URL empty do nothing!
        if (!scriptAddress.isEmpty()) {
            def slurper = new JsonSlurper()
            def profile = slurper.parseText(jsonProfile)
            def profileEnv = groovy.json.JsonOutput.toJson(profile) //format JSON profile for send to ENVs

            Config config = new ConfigBuilder().withMasterUrl(this.k8sAPIURL).withTrustCerts(true).withCaCertData(this.k8sCAcert).withOauthToken(this.k8sToken).build()
            KubernetesClient client = new DefaultKubernetesClient(config)

            // Create an RC
            def reqmap = [:]
            reqmap.put("memory", new Quantity(memoryLimitMegabytes.toString() + "Mi"))
            def reqs = new ResourceRequirements(reqmap, reqmap)

            def envs = [EnvVar]
            envs = [new EnvVar('loadScriptURL', "${scriptAddress.toString()}?private_token=${this.gitToken}".toString(), null),
                    new EnvVar('loadProfileName', "-JProfileName=${profileName}".toString(), null),
                    new EnvVar('loadIntencity', "-JIntencity=${intensityPerHour}".toString(), null),
                    new EnvVar('loadHoldload', "-JHoldload=${loadingTime}".toString(), null),
                    new EnvVar('loadPacing', "-JPacing=${pacing}".toString(), null),
                    new EnvVar('loadHostName', '-JHostName=k8s', null),
                    new EnvVar('loadRunId', "-JRunId=${runId}".toString(), null),
                    new EnvVar('loadInfluxHost', "-JInfluxHostName=${this.influxHost}".toString(), null),
                    new EnvVar('loadInfluxPort', "-JInfluxPort=${this.influxPort}".toString(), null),
                    new EnvVar('loadInfluxLogin', "-JInfluxLogin=${this.influxLogin}".toString(), null),
                    new EnvVar('loadInfluxPassword', "-JInfluxPassword=${this.influxPassword}".toString(), null),
                    new EnvVar('TZ', this.outTimeZone, null),
                    new EnvVar('UserName', this.userName, null),
                    new EnvVar('ProfileName', profileName.toString(), null),
                    new EnvVar('RunId', runId.toString(), null),
                    new EnvVar('ScriptName', serviceName.toString(), null),
                    new EnvVar('ScriptIntensity', intensityPerHour.toString(), null),
                    new EnvVar('ScriptURL', scriptAddress.toString(), null),
                    new EnvVar('RunType', 'k8s', null),
                    new EnvVar('ProfileJSON', "'$profileEnv'", null),
                    new EnvVar('Holdload', loadingTime.toString(), null),
                    new EnvVar('Pacing', pacing, null),
                    new EnvVar('ScriptPosition', scriptPosition, null),
                    new EnvVar('ClickHouseURL', this.clickHouseUrl, null),
                    new EnvVar('ClickHouseLogin', this.clickHouseLogin, null),
                    new EnvVar('ClickHousePassword', this.clickHousePassword, null),
                    new EnvVar('InfluxDBURL', this.influxHost + ':' + this.influxPort, null),
                    new EnvVar('InfluxDBLogin', this.influxLogin, null),
                    new EnvVar('InfluxDBPassword', this.influxPassword, null),
                    new EnvVar('LokiEndpoint', this.lokiUrl, null),
                    new EnvVar('LokiAuth', "Basic ${this.lokiAuth.bytes.encodeBase64().toString()}", null),
                    new EnvVar('LazyListener', this.lazyListener, null),
                    new EnvVar('LazyListenerURL', this.lazyListenerURL, null),
            ]


            if (!jksVar.isEmpty()) {
                envs.add(new EnvVar('jksFileBase', jksVar, null))
                envs.add(new EnvVar('jksPassword', this.jksPassword, null))
                envs.add(new EnvVar('containerNum', "1", null))
            }

            //обогащаем дополнительными, если есть чем
            profile["Parameters list"].eachWithIndex
                    { it, index ->
                        envs.add(new EnvVar("param$index", "-${it["Parameter name"]}=${it["Parameter value"]}".toString(), null))
                    }
            ReplicationController rc = new ReplicationControllerBuilder()
                    .withNewMetadata().withName("${this.loaderId}${cleanString(serviceName)}").addToLabels("service", "testload").endMetadata()
                    .withNewSpec().withReplicas(copies.toInteger())
                    .withNewTemplate()
                    .withNewMetadata()
                    .addToLabels("servicetype", "testload")
                    .addToLabels("runid", runId.toString())
                    .addToLabels("profilename", profileName.toString())
                    .addToLabels("memoryrequirement", memoryLimitMegabytes.toString())
                    .addToLabels("jobnum", this.loaderId)
                    .endMetadata()
                    .withNewSpec()
                    .addNewContainer().withName("testload").withImage(this.jmeterBaseImage)
                    .withEnv(envs)
                    .withResources(reqs)
                    .endContainer()
                    .endSpec()
                    .endTemplate()
                    .endSpec().build()

            client.replicationControllers().inNamespace(k8snamespace).create(rc)

            return null
        }
        return null
    }

    /**
     * --------------------------------------------------------------------------------------
     * Loki part
     */
    def lokiUrl="http://localhost:3100/api/prom/push"
    def lokiAuth="logger:password"

    static def cleanString(inputString) {
        return inputString.replaceAll("[\\s\\*\\_\\-+.^:,]", "")
    }
}