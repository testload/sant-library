package cloud.testload

import spock.lang.Specification

class SANTLibGitlabTest extends Specification {

    def library=new SANTLib()

//    void setup() {
//        //clickhouse
//        library.setClickHouseUrl(System.getenv("CH_URL"))
//        library.setClickHouseLogin(System.getenv("CH_LOGIN"))
//        library.setClickHousePassword(System.getenv("CH_PASSWORD"))
//        //gitlab
//        library.setGitToken(System.getenv("GITLAB_TOKEN"))
//        library.setGitlabWikiProjectId(System.getenv("GITLAB_PROJECTID"))
//        //k8s
//        library.setK8sAPIURL(System.getenv("K8S_URL"))
//        library.setK8sCAcert(System.getenv("K8S_CERT"))
//        library.setK8sToken(System.getenv("K8S_TOKEN"))
//        //confluence
//        library.setConfluenceCredentials(System.getenv("CONF_CREDENTIALS"))
//        library.setConfluenceBasePageId(System.getenv("CONF_BASEID"))
//        library.setConfluenceHost(System.getenv("CONF_URL"))
//        library.setConfluenceSpace(System.getenv("CONF_SPACE"))
//    }
//
//    void cleanup() {
//    }
//
//    def "GitlabWikiGetProfilesList"() {
//        def list=library.gitlabWikiGetProfilesList()
//
//        expect:
//        list.contains('sant')
//    }
//
//    def "GitlabWikiProfileBody"() {
//        def htmlBody=library.gitlabWikiGetProfileBodyByName('sant')
//        def jsonBody=library.gitlabWikiParseProfileBody(htmlBody)
//
//        expect:
//        htmlBody.contains('json_test.jmx) | Testing direct JSON settings | 2000 | 30 | 2 | 300')
//        jsonBody.contains('"Intensity per hour":"2000","Pacing in seconds":"30","Containers":"2","Memory requirement":"300","Restart policy":"any","JKS base filename":""')
//    }
}
