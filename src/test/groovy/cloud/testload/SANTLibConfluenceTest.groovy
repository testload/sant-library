package cloud.testload

import spock.lang.Specification

class SANTLibConfluenceTest extends Specification {

    def library=new SANTLib()

    void setup() {
        //clickhouse
        library.setClickHouseUrl(System.getenv("CH_URL"))
        library.setClickHouseLogin(System.getenv("CH_LOGIN"))
        library.setClickHousePassword(System.getenv("CH_PASSWORD"))
        //gitlab
        library.setGitToken(System.getenv("GITLAB_TOKEN"))
        library.setGitlabWikiProjectId(System.getenv("GITLAB_PROJECTID"))
        //k8s
        library.setK8sAPIURL(System.getenv("K8S_URL"))
        library.setK8sCAcert(System.getenv("K8S_CERT"))
        library.setK8sToken(System.getenv("K8S_TOKEN"))
        //confluence
        library.setConfluenceCredentials(System.getenv("CONF_CREDENTIALS"))
        library.setConfluenceBasePageId(System.getenv("CONF_BASEID"))
        library.setConfluenceHost(System.getenv("CONF_URL"))
        library.setConfluenceSpace(System.getenv("CONF_SPACE"))
    }

    void cleanup() {
    }

    def "ConfluenceProfileBody"() {
        def htmlBodyId=library.confluenceGetProfileIdByName('LT:Profile1')
        def htmlBody=library.confluenceGetProfileBodyById(htmlBodyId)
        def jsonBody=library.confluenceParseProfileBody(htmlBody)

        expect:
        htmlBodyId=='360449'
        htmlBody.contains('<ac:structured-macro ac:name="excerpt" ac:schema-version="1" data-layout="default" ac:macro-id="5c867994-4498-42c4-8246-fe7e2e100022"><ac:rich-text-body><p>Simple load testing example</p></ac:rich-text-body></ac:structured-macro><p><strong>Длительность(минут):10</strong></p><table data-layout="wide"><colgroup><col style="width: 189.0px;" /><col style="width: 113.0px;" /><col style="width: 109.0px;" /><col style="width: 137.0px;" /><col style="width: 137.0px;" /><col style="width: 137.0px;" /><col style="width: 137.0px;" /></colgroup><tbody><tr><th><p style="text-align: center;"><strong>Код сценария (ссылка на скрипт)</strong></p></th><th><p style="text-align: center;"><strong>Описание сценария</strong></p></th><th><p style="text-align: center;"><strong>Интенсивность (операций в час на поток)</strong></p></th><th><p style="text-align: center;"><strong>Плановое время выполнения операции</strong></p><p style="text-align: center;"><strong>(секунд)</strong></p></th><th><p style="text-align: center;"><strong>Количество</strong></p><p style="text-align: center;"><strong>потоков</strong></p></th><th><p style="text-align: center;"><strong>Сколько памяти выделять на поток</strong></p><p style="text-align: center;"><strong>(мегабайт)</strong></p></th><th><p style="text-align: center;"><strong>Имя файла с сертификатами</strong></p></th></tr><tr><td><p><a href="https://gitlab.com/testload/examplescripts/raw/master/Scripts/hit_google_test_clickhouse.jmx">hit_google_stats_to_clickhouse</a></p></td><td><p>Отправляем запрос в Google, схраняем статистику в ClickHouse</p></td><td><p>1000</p></td><td><p>10</p></td><td><p>1</p></td><td><p>500</p></td><td><p><br /></p></td></tr></tbody></table><p><br /><strong>Общие параметры</strong></p><table data-layout="wide"><colgroup><col style="width: 155.0px;" /><col style="width: 804.0px;" /></colgroup><tbody><tr><th><p>Имя для Jmeter</p></th><th><p>Значение</p></th></tr><tr><td><p>JThreads</p></td><td><p>4</p></td></tr></tbody></table><p><br /><strong>Результаты:</strong></p><ac:structured-macro ac:name="children" ac:schema-version="2" data-layout="default" ac:macro-id="915d1d86-71ba-454b-936d-76324ab6c61a"><ac:parameter ac:name="style">h2</ac:parameter><ac:parameter ac:name="excerptType">rich content</ac:parameter></ac:structured-macro><ac:structured-macro ac:name="children" ac:schema-version="2" data-layout="default" ac:macro-id="8304a0b0-7328-435a-8798-126171105b6b"><ac:parameter ac:name="style">h2</ac:parameter><ac:parameter ac:name="excerptType">rich content</ac:parameter></ac:structured-macro>')
        jsonBody.contains('{"Loading in minutes":"10","Description":"<p>Simple load testing example</p>","Scripts list":[{"Script name":"hit_google_stats_to_clickhouse","Script link":"https://gitlab.com/testload/examplescripts/raw/master/Scripts/hit_google_test_clickhouse.jmx","Intensity per hour":"1000","Pacing in seconds":"10","Containers":"1","Memory requirement":"500","Restart policy":"any","JKS base filename":""}],"Parameters list":[{"Parameter name":"JThreads","Parameter value":"4"}]}')
    }

}
