package cloud.testload

import spock.lang.Specification

class SANTLibOtherFunctionsTest extends Specification {

    def library=new SANTLib()

    void setup() {
    }

    void cleanup() {
    }

    def "GetTranslateForDefault"() {
        expect:
        library.getTranslateFor(input) == output

        where:
        input    | output
        "num"               | "№"
        "name"              | "Код процесса"
        "intensityHour"     | "Полученная интенсивность (операций/час)"
        "intensitySec"      | "Полученная интенсивность (операций/сек)"
        "profileReached"    | "Попадание в профиль (%)"
        "avgRespTime"       | "Среднее время отклика (секунд)"
        "respTime97Perc"    | "Время отклика 97% (не превышает секунд)"
        "errorRate"         | "Процент ошибок"
        "wakingUp"          | "Просыпаемся..."
        "totalCountHTML"    | "Общее количество<br />за время выполнения"
        "intensityHourHTML" | "Полученная интенсивность<br />(операций/час)"
        "intensitySecHTML"  | "Полученная интенсивность<br />(операций/сек)"
        "profileReachedHTML"| "Попадание в профиль<br />(%)"
        "avgRespTimeHTML"   | "Среднее время отклика<br />(секунд)"
        "respTime97PercHTML"| "Время отклика 97%<br />(не превышает секунд)"
        "scenarioName"      | "Код сценария (ссылка на скрипт)"
        "scenarioDescription"                | "Описание сценария"
        "intensityPlannedPerHourPerContainer"| "Интенсивность (операций в час на поток)"
        "containers"        | "Количество потоков"
        "description"       | "Описание профиля"
        "starttime"         | "Начало"
        "stoptime"          | "Завершение"
        "holdload"          | "Длительность(минут)"
        "username"          | "Пользователь"
        "comment"           | "Комментарий:"
        "hlparse"           | "Длительность(минут):"
        "additionals"        | "Дополнительно:"
    }

    def "GetTranslateForRu"() {
        library.setLanguage("ru")
        expect:
        library.getTranslateFor(input) == output

        where:
        input    | output
        "num"               | "№"
        "name"              | "Код процесса"
        "intensityHour"     | "Полученная интенсивность (операций/час)"
        "intensitySec"      | "Полученная интенсивность (операций/сек)"
        "profileReached"    | "Попадание в профиль (%)"
        "avgRespTime"       | "Среднее время отклика (секунд)"
        "respTime97Perc"    | "Время отклика 97% (не превышает секунд)"
        "errorRate"         | "Процент ошибок"
        "wakingUp"          | "Просыпаемся..."
        "totalCountHTML"    | "Общее количество<br />за время выполнения"
        "intensityHourHTML" | "Полученная интенсивность<br />(операций/час)"
        "intensitySecHTML"  | "Полученная интенсивность<br />(операций/сек)"
        "profileReachedHTML"| "Попадание в профиль<br />(%)"
        "avgRespTimeHTML"   | "Среднее время отклика<br />(секунд)"
        "respTime97PercHTML"| "Время отклика 97%<br />(не превышает секунд)"
        "scenarioName"      | "Код сценария (ссылка на скрипт)"
        "scenarioDescription"                | "Описание сценария"
        "intensityPlannedPerHourPerContainer"| "Интенсивность (операций в час на поток)"
        "containers"        | "Количество потоков"
        "description"       | "Описание профиля"
        "starttime"         | "Начало"
        "stoptime"          | "Завершение"
        "holdload"          | "Длительность(минут)"
        "username"          | "Пользователь"
        "comment"           | "Комментарий:"
        "hlparse"           | "Длительность(минут):"
        "additionals"        | "Дополнительно:"
    }

    def "GetTranslateForEn"() {
        library.setLanguage("en")
        expect:
        library.getTranslateFor(input) == output

        where:
        input    | output
        "num"               | "#"
        "name"              | "Transaction"
        "intensityHour"     | "Intensity per hour"
        "intensitySec"      | "Intensity per second"
        "profileReached"    | "Percent profile reached"
        "avgRespTime"       | "Average response time"
        "respTime97Perc"    | "Response time 97 percentile"
        "errorRate"         | "Error rate"
        "wakingUp"          | "Waking up..."
        "totalCountHTML"    | "Total count"
        "intensityHourHTML" | "Intensity<br />(ops/hour)"
        "intensitySecHTML"  | "Intensity<br />(ops/sec)"
        "profileReachedHTML"| "Profile reached<br />(%)"
        "avgRespTimeHTML"   | "Average response time<br />(sec)"
        "respTime97PercHTML"| "Response time 97 percentile<br />(sec)"
        "scenarioName"      | "Transaction (script URL)"
        "scenarioDescription"                | "Transaction description"
        "intensityPlannedPerHourPerContainer"| "Planned intensity (per hour X per container)"
        "containers"        | "Containers"
        "description"       | "Profile description"
        "starttime"         | "Start time"
        "stoptime"          | "End time"
        "holdload"          | "Loading(minutes)"
        "username"          | "User name"
        "comment"           | "Commentary:"
        "hlparse"           | "Duration(minutes):"
        "additionals"       | "Additional:"
    }

    def "CleanString"() {
        expect:
        library.cleanString(input) == output

        where:
        input    | output
        "test"  | "test"
        "ScRipt"   | "ScRipt"
        "test _*-+^.:,script" | "testscript"
        "testscript _*-+^.:,"  | "testscript"
        "te _*-+^.:,st _*-+^.:,scri_*-+^.:,pt" | "testscript"
        "_*-+^.:,t_*-+^.:est_+^.:,s_+^.:,cr_*-+^.:ipt"  | "testscript"
        " _*-+^.:,testscript"  | "testscript"
    }
}
